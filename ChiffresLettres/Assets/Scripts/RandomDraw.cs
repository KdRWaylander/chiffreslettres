﻿using System.Collections.Generic;
using UnityEngine;

public class RandomDraw : MonoBehaviour
{
    [SerializeField] private Letter[] m_ConsonantDictionnary;
    [SerializeField] private Letter[] m_VowelsDictionnary;

    private List<int> m_NumbersList;

    public int TargetNumber { get; private set; }
    public List<string> NumbersList { get; private set; }
    public List<string> LettersList { get; private set; }

    private void Start()
    {
        m_NumbersList = new List<int> { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 25, 50, 75, 100};

        LettersList = new List<string>();
        NumbersList = new List<string>();

        ClearLettersList();
    }

    public void DrawNewConsonant()
    {
        if (LettersList.Count >= 10)
            return;

        LettersList.Add(WeightedLetterChoice(m_ConsonantDictionnary));

        // trigger event
        GameManager.Instance.HandleNewLetterListDrawn();
    }

    public void DrawNewVowel()
    {
        if (LettersList.Count >= 10)
            return;

        LettersList.Add(WeightedLetterChoice(m_VowelsDictionnary));

        // trigger event
        GameManager.Instance.HandleNewLetterListDrawn();
    }

    public void ClearLettersList()
    {
        LettersList.Clear();

        // trigger event
        GameManager.Instance.HandleNewLetterListDrawn();
    }

    public void DrawNewNumbersListAndTargetNumber()
    {
        NumbersList.Clear();
        m_NumbersList.FisherYatesShuffle();

        for (int i = 0; i < 6; i++)
        {
            NumbersList.Add(m_NumbersList[i].ToString());
        }

        TargetNumber = Random.Range(101, 1000);

        // trigger event
        GameManager.Instance.HandleNewNumberListDrawn();
    }

    private string WeightedLetterChoice(Letter[] characters)
    {
        string character = "";

        // List of the cumulative sum of weights
        float[] sumOfChances = new float[characters.Length];

        // Fill the list + handling the case with only 1 weight
        sumOfChances[0] = characters[0].density;
        if (characters.Length > 1)
        {
            for (int i = 1; i < characters.Length; i++)
            {
                sumOfChances[i] = sumOfChances[i - 1] + characters[i].density;
            }

            // Pick a random number between 0 and the total sum of weights
            float r = Random.value * sumOfChances[characters.Length - 1];

            // Find the index of the last item whose cumulative sum is above the random number
            // Pick the asset at index and return it
            for (int i = 0; i < characters.Length; i++)
            {
                if (r <= sumOfChances[i])
                {
                    character = characters[i].character;
                    break;
                }
            }
        }
        else // In case there is only 1 item in the list
        {
            character = characters[0].character;
        }

        return character;
    }
}

[System.Serializable]
public struct Letter
{
    public string character;
    public float density;
}