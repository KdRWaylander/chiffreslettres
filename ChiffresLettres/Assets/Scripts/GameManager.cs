﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public event Action NewLetterListDrawn;
    public event Action NewNumberListDrawn;

    private void Awake()
    {
        Instance = this;
    }

    public void HandleNewLetterListDrawn()
    {
        if (NewLetterListDrawn != null)
            NewLetterListDrawn();
    }

    public void HandleNewNumberListDrawn()
    {
        if (NewNumberListDrawn != null)
            NewNumberListDrawn();
    }
}