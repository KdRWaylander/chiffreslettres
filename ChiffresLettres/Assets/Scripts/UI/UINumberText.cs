﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UINumberText : MonoBehaviour
{
    [SerializeField]
    private int m_NumberIndex;

    private TextMeshProUGUI m_Text;
    private RandomDraw m_RandomDraw;

    private void Awake()
    {
        m_RandomDraw = FindObjectOfType<RandomDraw>();
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.Instance.NewNumberListDrawn += UpdateText;
    }

    private void UpdateText()
    {
        m_Text.text = m_RandomDraw.NumbersList[m_NumberIndex].ToString();
    }
}