﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UILetterText : MonoBehaviour
{
    [SerializeField]
    private int m_LetterIndex;

    private TextMeshProUGUI m_Text;
    private RandomDraw m_RandomDraw;

    private void Awake()
    {
        m_RandomDraw = FindObjectOfType<RandomDraw>();
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.Instance.NewLetterListDrawn += UpdateText;
    }

    private void UpdateText()
    {
        if (m_RandomDraw.LettersList.Count < m_LetterIndex + 1)
            m_Text.text = "";
        else
            m_Text.text = m_RandomDraw.LettersList[m_LetterIndex].ToString();
    }
}