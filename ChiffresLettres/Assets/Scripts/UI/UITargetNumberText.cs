﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UITargetNumberText : MonoBehaviour
{
    private TextMeshProUGUI m_Text;
    private RandomDraw m_RandomDraw;

    private void Awake()
    {
        m_RandomDraw = FindObjectOfType<RandomDraw>();
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.Instance.NewNumberListDrawn += UpdateText;
    }

    private void UpdateText()
    {
        m_Text.text = m_RandomDraw.TargetNumber.ToString();
    }
}