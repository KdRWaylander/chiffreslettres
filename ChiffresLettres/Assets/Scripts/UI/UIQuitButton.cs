﻿using UnityEngine;

public class UIQuitButton : MonoBehaviour
{
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}