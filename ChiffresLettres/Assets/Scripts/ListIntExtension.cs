﻿using System.Collections.Generic;

public static class ListIntExtension
{
    public static List<int> FisherYatesShuffle(this List<int> list)
    {
        int a, j;
        for (int i = list.Count - 1; i > 0; i--)
        {
            j = UnityEngine.Random.Range(0, i + 1);
            a = list[i];
            list[i] = list[j];
            list[j] = a;
        }
        return list;
    }
}